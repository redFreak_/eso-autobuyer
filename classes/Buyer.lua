function RF_AB.Buyer.getOrderEntryStore(name, price, itemLink, icon, index, stack)
    return RF_AB.Buyer.getOrderEntry('Store', name, price, itemLink, icon, index, stack)
end

function RF_AB.Buyer.getOrderEntryGuild(name, price, itemLink, icon, index, stack)
    return RF_AB.Buyer.getOrderEntry('Guild', name, price, itemLink, icon, index, stack)
end

function RF_AB.Buyer.getOrderEntry(context, name, price, itemLink, icon, index, stack)
    RF_AB:registerItem(name, price, itemLink, icon, context)
    local totalStackSize = RF_AB.Buyer.getItemStockByItemLink(itemLink)
    local itemConfig = RF_AB:getItemConfig(name)

    -- if there is an item config create the entry
    if (itemConfig ~= nil) then
        local strategy = RF_AB:getStrategy(context, name)
        return strategy.createOrderEntry(price, itemConfig, totalStackSize, index, stack) -- base checks included
    else
        -- nil: no config saved
    end
end

function RF_AB.Buyer.getAmount(goldToSpend, price, amountWanted)
    local amountPayable = math.floor(goldToSpend / price)
    if (amountWanted <= amountPayable) then
        return amountWanted
    else
        return amountPayable
    end
end

function RF_AB.Buyer.getGoldToSpend(silent)
    local gold = GetCarriedCurrencyAmount(CURT_MONEY);
    local goldToSpend = gold - RF_AB.savedVariables.keepMoney
    if (goldToSpend < 0) then goldToSpend = 0 end

    -- if (silent ~= true) then RF_AB.Utils.message(RF_AB.Utils:MoneyString(gold) .. 'g in inventory, ' .. RF_AB.Utils:MoneyString(RF_AB
    -- .savedVariables.keepMoney) .. 'g to keep, ' .. RF_AB.Utils:MoneyString(goldToSpend) .. 'g to spend') end
    return goldToSpend
end

function RF_AB.Buyer.adjustOrderEqual(order, goldToSpend)
    -- get order total
    local orderTotal = 0
    for name, orderItem in pairs(order) do
        orderTotal = orderTotal + (orderItem.price * orderItem.amount)
    end
    if (orderTotal <= goldToSpend) then return order end
    -- adjust order
    local factor = goldToSpend / orderTotal
    for name, orderItem in pairs(order) do
        local amount_new = math.floor(order[name].amount * factor)
        --RF_AB.Utils.warn('Adjusting order  ' .. orderItem.itemLink .. ' from ' .. orderItem.amount .. ' to ' ..
        --        amount_new .. ' not enough money')
        order[name].amount = amount_new
    end

    return order
end

function RF_AB.Buyer.getItemStockByItemLink(itemLink)
    local stackCountBackpack, stackCountBank, stackCountCraftBag = GetItemLinkStacks(itemLink)
    return stackCountBackpack + stackCountBank + stackCountCraftBag
end

RF_AB.Buyer.Store = {}
RF_AB.Buyer.Guild = {}


