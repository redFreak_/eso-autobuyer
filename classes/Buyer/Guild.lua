function RF_AB.Buyer.Guild.registerEvents()
    EVENT_MANAGER:RegisterForEvent("AB_OpenGuildStore", EVENT_OPEN_TRADING_HOUSE, RF_AB.Buyer.Guild.StoreOpened)
    -- receive search results by AGS
    RF_AB.Buyer.Guild.AGS:RegisterCallback(RF_AB.Buyer.Guild.AGS.callback.SEARCH_RESULTS_RECEIVED, RF_AB.Buyer.Guild.buyItems)
    -- get the TradingHouseWrapper to directly "communicate" with AGS
    RF_AB.Buyer.Guild.AGS:RegisterCallback(RF_AB.Buyer.Guild.AGS.callback.AFTER_INITIAL_SETUP, RF_AB.Buyer.Guild.registerTradingHouseWrapper)
    RF_AB.Buyer.Guild.AGS:RegisterCallback(RF_AB.Buyer.Guild.AGS.callback.ITEM_PURCHASED, function(itemData)
        -- set it bought
        itemData.purchased = true
        -- refresh search result
        RF_AB.Buyer.Guild.TradingHouseWrapper.searchTab.searchResultList:RefreshVisible()
    end)

    EVENT_MANAGER:RegisterForEvent("AB_OpenGuildStore", EVENT_CLOSE_TRADING_HOUSE, RF_AB.Buyer.Guild.StoreClosed)
end

function RF_AB.Buyer.Guild.registerTradingHouseWrapper(Wrapper)
    RF_AB.debug('AGS after init callback')
    RF_AB.Buyer.Guild.TradingHouseWrapper = Wrapper
end

function RF_AB.Buyer.Guild.StoreOpened()
    -- nil
end

function RF_AB.Buyer.Guild.StoreClosed()
    -- nil
end

function RF_AB.Buyer.Guild.buyItems(pendingGuildName, numItems, page, hasMore, guildId)
    -- RF_AB.Utils.message('Processing ' .. numItems .. ' search results by ' .. pendingGuildName)
    if (not RF_AB.savedVariables.guildEnabled) then return end
    local preOrders = {} -- [name][uniqueId]
    -- collect item data
    for x = 1, numItems do
        local itemLink = GetTradingHouseSearchResultItemLink(x, LINK_STYLE_DEFAULT)
        if (RF_AB.Utils.isSupportedItemFilterType(GetItemLinkFilterTypeInfo(itemLink))) then
            local icon, name, _, stack, seller, _, _, currencyType, uniqueId, pricePerUnit = GetTradingHouseSearchResultItemInfo(x)
            if (currencyType == CURT_MONEY or currencyType == CURT_NONE) then
                local order_entry = RF_AB.Buyer.getOrderEntryGuild(name, pricePerUnit, itemLink, icon, x, stack)
                if (order_entry ~= nil) then
                    -- mixin some more infos
                    order_entry.seller = seller
                    order_entry.guildName = pendingGuildName
                    -- "sort" by item_name
                    if (preOrders[name] == nil) then preOrders[name] = {} end
                    preOrders[name][uniqueId] = order_entry
                end
            end
        end
    end

    local goldToSpend = RF_AB.Buyer.getGoldToSpend()
    local orders = RF_AB.Buyer.Guild.filterPreOrder(preOrders, goldToSpend)

    RF_AB.Buyer.Guild.buy(orders, goldToSpend, guildId)
end

function RF_AB.Buyer.Guild.buy(order, goldToSpend, guildId)
    for _, orderItem in pairs(order) do
        if (goldToSpend <= 0) then
            RF_AB.Utils.error('Not buying ' .. orderItem.itemLink .. ' no money left.')
        else
            if (goldToSpend >= orderItem.stack * orderItem.price) then
                RF_AB.Utils.success('Buying ' .. orderItem.stack .. 'x ' .. orderItem.itemLink .. ' @ ' .. RF_AB.Utils:MoneyString(orderItem.price))
                -- prepare data for AGS
                local itemData, _ = RF_AB.Buyer.Guild.TradingHouseWrapper.itemDatabase:TryGetItemDataInCurrentGuildByUniqueId(orderItem.uniqueId)
                -- call AGS to handle purchase
                local purchaseActivity = RF_AB.Buyer.Guild.TradingHouseWrapper.activityManager:PurchaseItem(guildId, itemData)
                --                purchaseActivity.pendingPromise:Then(function()
                --                    -- set it bought
                --                    itemData.purchased = true
                --                    -- refresh search result
                --                    RF_AB.Buyer.Guild.TradingHouseWrapper.searchTab.searchResultList:RefreshVisible()
                --                end)
            else
                RF_AB.Utils.error('Not buying ' .. orderItem.itemLink .. ' no money left.')
            end

            goldToSpend = goldToSpend - (orderItem.stack * orderItem.price)
        end
    end
end

function RF_AB.Buyer.Guild.filterPreOrder(preOrders, goldToSpend)
    -- nested sorting function
    local function compareOrders(orderA, orderB)
        return orderA.price < orderB.price
    end

    local filteredOrders = {}
    for name, orders in pairs(preOrders) do
        local itemConfig = RF_AB:getItemConfig(name)
        itemConfig.pendingOrder = 0
        local itemSum = 0

        table.sort(orders, compareOrders)
        for uniqueId, orderItem in pairs(orders) do
            if (goldToSpend - itemSum <= 0) then
                RF_AB.Utils.warn('Not buying ' .. orderItem.itemLink .. ' no money left.')
            else
                local strategy = RF_AB:getStrategy('Guild', name)
                local amountToBuy = strategy.getAmount(goldToSpend, orderItem, itemConfig)

                -- if the strategy decided to buy then put in order
                if (amountToBuy > 0) then
                    orderItem.uniqueId = uniqueId
                    table.insert(filteredOrders, orderItem)
                    itemSum = itemSum + (orderItem.price * orderItem.stack)
                    itemConfig.pendingOrder = itemConfig.pendingOrder + orderItem.stack
                end
            end
        end
    end

    return filteredOrders
end

function RF_AB.Buyer.Guild.fillStorageStrategy(preOrder, goldToSpend)
end

function RF_AB.Buyer.Guild.spendGoldStrategy(preOrder, goldToSpend)
end

function RF_AB.Buyer.Guild.bestDealStrategy(preOrder, goldToSpend)
end

RF_AB.Buyer.Guild.AGS = AwesomeGuildStore
RF_AB.Buyer.Guild.TradingHouseWrapper = nil
RF_AB.Buyer.Guild.registerEvents()
