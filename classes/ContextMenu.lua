local guildStrategyBefore
-- hook for all but store context
local function show(inventorySlot, slotAction)
    local bagId, slotIndex = ZO_Inventory_GetBagAndIndex(inventorySlot)
    local icon, name, stack, pricePerUnit, itemLink, context = RF_AB.Utils.getItemInfo(bagId, slotIndex, inventorySlot)

    if (not (itemLink)) then return end

    -- if it's bound we cannot use it
    --if not (IsItemLinkBound(itemLink)) then
    --    RF_AB.Utils.error('not IsItemLinkBound')
    --    return
    --end

    -- only allow crafting and misc items
    if not (RF_AB.Utils.isSupportedItemFilterType(GetItemLinkFilterTypeInfo(itemLink))) then
        RF_AB.Utils.error('wrong filter ' .. GetItemLinkFilterTypeInfo(itemLink))
        return
    end

    -- if it's in inventory and not bound than it must be tradeable by guild
    RF_AB:registerItem(name, pricePerUnit, itemLink, icon, 'Guild')
    -- register menu
    AddCustomMenuItem('Auto Buyer settings', function() RF_AB.ContextMenu:Callback(name) end)
end

function RF_AB.ContextMenu:Register()
    local LCM = LibCustomMenu
    LCM:RegisterContextMenu(show, LCM.CATEGORY_PRIMARY)
end

local function RF_AB_SettingDialog_CloseButton_OnClicked()
    -- todo: save data
    ZO_PopupTooltip_Hide()
    RF_AB_SettingsDialog:SetHidden(true)
end

local RF_AB_SettingDialog_Fields = {}
local currentConfig

local function RF_AB_SettingDialog_itemField_OnValueChanged(self, value, reason)
    local field_name_price = self:GetName():gsub('_Enable', '_Price')
    local field_name_amount = self:GetName():gsub('_Enable', '_Amount')
    RF_AB_SettingDialog_Fields[field_name_price]:UpdateDisabled()
    RF_AB_SettingDialog_Fields[field_name_amount]:UpdateDisabled()
end

function RF_AB.ContextMenu.GetDescriptionText(itemConfig)
    return itemConfig.itemLink
end

function RF_AB.ContextMenu.GetSalesTrackerText(itemConfig)
    -- todo: add TTC, MM and ATT support
    return ''
end

local function RF_AB_SettingDialog_itemField_OnStrategyChanged()
    -- hide all controls
    for _, control in pairs(RF_AB_SettingDialog_Fields) do
        control:SetHidden(true)
    end

    -- rerender
    RF_AB.ContextMenu.showSettingsDialog(currentConfig)
end

local function injectDataToLAMField(control, controlData)
    -- common data
    control.label:SetText(controlData.name)
    control.data.tooltipText = controlData.tooltip
    control.data.setFunc = controlData.setFunc
    control.data.getFunc = controlData.getFunc
    control.data.disabled = controlData.disabled

    if (controlData.type == 'checkbox') then
        -- inject our self for OnMouseUp - currently only Enabled is checkbox
        local origHandler = control:GetHandler('OnMouseUp')
        control:SetHandler('OnMouseUp', function(self, value, eventReason)
            origHandler(control)
            RF_AB_SettingDialog_itemField_OnValueChanged(self, value, eventReason)
        end)
    elseif (controlData.type == 'dropdown' and controlData.name == 'strategy') then
        -- hook the set function for the strategy to refresh rerender the dialog
        local origSetFunc = control.data.setFunc
        control.data.setFunc = function(value)
            origSetFunc(value)
            RF_AB_SettingDialog_itemField_OnStrategyChanged()
        end
    end
    control:UpdateValue()
    control:UpdateDisabled()

    LibAddonMenu2.util.RequestRefreshIfNeeded(control)
end

function RF_AB.ContextMenu.showSettingsDialog(itemConfig)
    local wm = WINDOW_MANAGER
    local tmp
    -- main window
    local dialog = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog"] or CreateTopLevelWindow("RF_AB_SettingsDialog")
    RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog"] = dialog
    dialog:SetHidden(false)
    dialog:SetAnchor(TOPLEFT, nil, TOPLEFT, 100, 150)
    dialog:SetDimensions(600, 400)
    dialog:SetMovable(false)
    -- mock some values to be able to be handeled by LAM
    dialog.data = { registerForDefaults = true, registerForRefresh = true }

    -- small close button
    local closeButton = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog_CloseButton"] or CreateControlFromVirtual("$(parent)_CloseButton",
        dialog, "ZO_CloseButton")
    RF_AB_SettingDialog_Fields[closeButton:GetName()] = closeButton
    closeButton:SetAnchor(TOPRIGHT, dialog, TOPRIGHT, 0, 0)
    closeButton:SetHandler("OnClicked", function(...) RF_AB_SettingDialog_CloseButton_OnClicked(dialog) end)
    closeButton:SetHidden(false)

    -- background
    local bg = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog_Background"] or dialog:CreateControl("$(parent)_Background", CT_TEXTURE)
    RF_AB_SettingDialog_Fields[bg:GetName()] = bg
    bg:SetTexture("EsoUI/Art/Miscellaneous/centerscreen_left.dds")
    bg:SetDimensions(680, 520)
    bg:SetAnchor(TOPLEFT, nil, TOPLEFT, -40, -40)
    bg:SetDrawLayer(DL_BACKGROUND)
    bg:SetExcludeFromResizeToFitExtents(true)
    bg:SetHidden(false)

    -- title
    local title = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog_Title"] or dialog:CreateControl("$(parent)_Title", CT_LABEL)
    RF_AB_SettingDialog_Fields[title:GetName()] = title
    title:SetAnchor(TOPLEFT, dialog, TOPLEFT, 10, 10)
    title:SetFont("ZoFontWinH1")
    title:SetText("|ccc0000red_Freak|r's Auto Buyer - " .. itemConfig.itemLink .. " settings")
    title:SetHidden(false)

    -- hr
    local divider = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog_Divider"] or wm:CreateControlFromVirtual("$(parent)_Divider", dialog,
        "ZO_Options_Divider")
    RF_AB_SettingDialog_Fields[divider:GetName()] = divider
    divider:SetAnchor(TOPLEFT, dialog, TOPLEFT, 10, 55)
    divider:SetHidden(false)

    -- item description
    local itemDescription = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog_Description"] or dialog:CreateControl("$(parent)_Description",
        CT_LABEL)
    RF_AB_SettingDialog_Fields[itemDescription:GetName()] = itemDescription
    itemDescription:SetDimensions(580, 20)
    tmp = RF_AB.ContextMenu.GetDescriptionText(itemConfig)
    itemDescription:SetText(tmp)
    itemDescription:SetAnchor(TOPLEFT, dialog, TOPLEFT, 10, 70)
    itemDescription:SetHidden(false)
    -- item description sales tracker
    local itemDescriptionSalesTracker = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog_Description_Sales_Tracker"] or
            dialog:CreateControl("$(parent)_Description_Sales_Tracker", CT_LABEL)
    RF_AB_SettingDialog_Fields[itemDescriptionSalesTracker:GetName()] = itemDescriptionSalesTracker
    itemDescriptionSalesTracker:SetDimensions(580, 20)
    tmp = RF_AB.ContextMenu.GetSalesTrackerText(itemConfig)
    itemDescriptionSalesTracker:SetText(tmp)
    itemDescriptionSalesTracker:SetAnchor(TOPLEFT, dialog, TOPLEFT, 10, 90)
    itemDescriptionSalesTracker:SetHidden(false)
    -- item description hint
    local itemDescriptionHint = RF_AB_SettingDialog_Fields["RF_AB_SettingsDialog_Description_Hint"] or
            dialog:CreateControl("$(parent)_Description_Hint", CT_LABEL)
    RF_AB_SettingDialog_Fields[itemDescriptionHint:GetName()] = itemDescriptionHint
    itemDescriptionHint:SetDimensions(580, 20)
    itemDescriptionHint:SetText('|cCC0000Values are changed directly, no save, no cancel.|r')
    itemDescriptionHint:SetAnchor(TOPLEFT, dialog, TOPLEFT, 10, 110)
    itemDescriptionHint:SetHidden(false)

    -- collect data for item fields
    local ItemControlDataStore = RF_AB.Settings:getItemFieldsByContext('Store', itemConfig.name)
    local ItemControlDataGuild = RF_AB.Settings:getItemFieldsByContext('Guild', itemConfig.name)

    -- display Store fields by LAM
    for i, controlData in pairs(ItemControlDataStore) do
        local name_internal = controlData.name_internal:gsub('${parent}', 'RF_AB_SettingsDialog')
        local control = RF_AB_SettingDialog_Fields[name_internal] or LAMCreateControl[controlData.type](dialog, controlData, name_internal)
        RF_AB_SettingDialog_Fields[control:GetName()] = control

        injectDataToLAMField(control, controlData)

        -- some markup
        control:SetDimensions(280, 40)
        control:SetAnchor(TOPLEFT, dialog, TOPLEFT, 10, (i - 1) * 60 + 80)
        control:SetHidden(false)
    end

    -- display Guild fields by LAM
    for i, controlData in pairs(ItemControlDataGuild) do
        local name_internal = controlData.name_internal:gsub('${parent}', 'RF_AB_SettingsDialog')
        local control = RF_AB_SettingDialog_Fields[name_internal] or LAMCreateControl[controlData.type](dialog, controlData, name_internal)
        RF_AB_SettingDialog_Fields[control:GetName()] = control

        injectDataToLAMField(control, controlData)

        -- some markup
        control:SetDimensions(280, 40)
        control:SetAnchor(TOPLEFT, dialog, TOPLEFT, 310, (i - 1) * 60 + 80)
        control:SetHidden(false)
    end
end

function RF_AB.ContextMenu:Callback(name)
    local itemConfig = RF_AB:getItemConfig(name)
    if (itemConfig == nil) then return end
    currentConfig = itemConfig

    -- show tooltip
    RF_AB.ContextMenu.ToolTipHandler = ZO_PopupTooltip_SetLink(itemConfig.itemLink)
    RF_AB.ContextMenu.showSettingsDialog(itemConfig)
end

RF_AB.ContextMenu.ToolTipHandler = nil
