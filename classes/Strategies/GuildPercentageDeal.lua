local MM = MasterMerchant

function RF_AB.Strategies.GuildPercentageDeal.getAmount(goldToSpend, orderItem, itemConfig)
    local pricePerUnit = orderItem.price
    local stack = orderItem.stack

    if not itemConfig['percentageGuild'] then
        return 0
    end

    -- test total price
    local priceStack = pricePerUnit * stack
    if (priceStack > goldToSpend) then
        -- RF_AB.Utils.error('PercentageDeal: Not buying ' .. stack .. 'x' .. itemConfig.itemLink .. '@' .. pricePerUnit .. 'g, no money left.')

        return 0
    end

    -- calculate the stack we want to buy based on the dealPercentage
    local mmItemLink = GetTradingHouseSearchResultItemLink(orderItem.index)
    local _, dealPercentage, _ = MM.GetDealInformation(mmItemLink, pricePerUnit * stack, stack)

    -- if no info set it very negative
    if (dealPercentage == nil) then
        dealPercentage = -99999
    end

    -- if it's no deal, it's no deal
    if (dealPercentage < itemConfig['percentageGuild']) then
        return 0
    end

    -- calculate wanted amount
    local adjustedX = (dealPercentage - itemConfig['percentageGuild'])
    local amountFactor = 0.01 * adjustedX * adjustedX + 1
    local amountWanted =  orderItem.amount * amountFactor - itemConfig.pendingOrder
    local inStock = RF_AB.Buyer.getItemStockByItemLink(itemConfig.itemLink)

     RF_AB.Utils.debug('PercentageDeal: ' .. stack .. 'x' .. itemConfig.itemLink .. '@' .. pricePerUnit .. 'g - ' .. inStock .. ' in-stock, I originally wanted ' .. orderItem.amount .. ', now I want ' .. amountWanted)
    if (amountWanted > stack) then
        return amountWanted
    else
        RF_AB.Utils.hint('PercentageDeal: Not buying ' .. stack .. 'x' .. itemConfig.itemLink .. ' @' .. pricePerUnit .. 'g, to much in inventory (max ' .. amountWanted .. ', ' .. itemConfig.pendingOrder .. ' in ordered')
    end

    return 0
end

-- makes a base check for the possible amount
function RF_AB.Strategies.GuildPercentageDeal.createOrderEntry(price, itemConfig, totalStackSize, index, stack)
    local possibleBuyStack = itemConfig['toBuyGuild']
    -- adjust amount if there is a possivle best deal
    if (itemConfig['toBuyGuild'] == nil) then
        RF_AB.Utils.error('PercentageDeal: can not get "toBuyGuild" for ' .. itemConfig.name)

        return nil
    end

    if (totalStackSize < possibleBuyStack) then
        local amountWantedOrig = (itemConfig['toBuyGuild'] - totalStackSize)
        if (amountWantedOrig < 0) then amountWantedOrig = 0 end
        local amountWanted = (possibleBuyStack - totalStackSize)
        if (amountWanted < 0) then amountWanted = 0 end

        local amountString = amountWantedOrig
        if (amountWanted ~= amountWantedOrig) then amountString = amountString .. 'x / ' .. amountWanted end

        RF_AB.Utils.success('PercentageDeal: preordering up to ' .. amountString .. 'x ' .. itemConfig.itemLink)

        return {
            amount = amountWanted,
            price = price,
            itemLink = itemConfig.itemLink,
            index = index,
            stack = stack
        }
    else
        RF_AB.Utils.hint('PercentageDeal: Not preordering ' .. itemConfig.itemLink .. ' enough in stock.')
    end
end

function RF_AB.Strategies.GuildPercentageDeal.getItemFields(item_name)
    return {
        RF_AB.Settings.getItemFieldByContext('Guild', item_name, 'Percentage'),
        RF_AB.Settings.getItemFieldByContext('Guild', item_name, 'Amount')
    }
end