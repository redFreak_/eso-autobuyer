-- makes a base check for the possible amount
function RF_AB.Strategies.StoreEqualBuy.createOrderEntry(price, itemConfig, totalStackSize, index, stack)
    if (price <= itemConfig['price' .. context]) then
        local possibleBuyStack = itemConfig['toBuy' .. context]
        if (possibleBuyStack == nil) then
            RF_AB.Utils.hint('Not processing ' .. itemConfig.itemLink .. ' wrong config.')
        elseif (totalStackSize < possibleBuyStack) then
            local amountWantedOrig = (itemConfig['toBuy' .. context] - totalStackSize)
            if (amountWantedOrig < 0) then amountWantedOrig = 0 end
            local amountWanted = (possibleBuyStack - totalStackSize)
            if (amountWanted < 0) then amountWanted = 0 end

            local amountString = amountWantedOrig
            if (amountWanted ~= amountWantedOrig) then amountString = amountString .. ' / ' .. amountWanted end

            RF_AB.Utils.success('ordering ' .. amountString .. 'x ' .. itemConfig.itemLink)

            return {
                amount = amountWanted,
                price = price,
                itemLink = itemConfig.itemLink,
                index = index,
                stack = stack
            }
        else
            RF_AB.Utils.hint('Not ordering ' .. itemConfig.itemLink .. ' enough in stock.')
        end
    else
        RF_AB.Utils.warn('Not ordering ' .. itemConfig.itemLink .. '@' .. price .. ' to expensive o.O.')
    end
end

function RF_AB.Strategies.StoreEqualBuy.getAmount(goldToSpend, orderItem, itemConfig)
    -- not handled here
    RF_AB.Utils.error('should not be called')
end

function RF_AB.Strategies.StoreEqualBuy.getItemFields(item_name)
    return {
        RF_AB.Settings.getItemFieldByContext('Store', item_name, 'Price'),
        RF_AB.Settings.getItemFieldByContext('Store', item_name, 'Amount')
    }
end