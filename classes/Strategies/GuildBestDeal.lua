function RF_AB.Strategies.GuildBestDeal.getAmount(goldToSpend, orderItem, itemConfig)
    local pricePerUnit = orderItem.price
    local stack = orderItem.stack
    local priceFactor = pricePerUnit / itemConfig.priceGuild
    local inStock = RF_AB.Buyer.getItemStockByItemLink(itemConfig.itemLink)
    local amountWanted = 0

    if (priceFactor > 1) then
        RF_AB.Utils.hint('BestDeal: Not buying ' .. stack .. 'x' .. itemConfig.itemLink .. '@' .. pricePerUnit .. 'g, to expensive')
        return 0
    elseif (priceFactor <= 0.333) then
        amountWanted = itemConfig.toBuyGuild * 10 - inStock - itemConfig.pendingOrder
    elseif (priceFactor < 0.5) then
        amountWanted = itemConfig.toBuyGuild * 5 - inStock - itemConfig.pendingOrder
    elseif (priceFactor < 0.66) then
        amountWanted = itemConfig.toBuyGuild * 2 - inStock - itemConfig.pendingOrder
    else
        amountWanted = math.floor(itemConfig.toBuyGuild / priceFactor) - inStock - itemConfig.pendingOrder
    end

    if (amountWanted >= stack) then return stack end

    RF_AB.Utils.hint('BestDeal: Not buying ' .. stack .. 'x' .. itemConfig.itemLink .. ' @' .. pricePerUnit .. 'g, to much in inventory (max ' .. amountWanted .. ', ' .. inStock - itemConfig.pendingOrder .. ' in stock and ordered')

    return 0
end

-- makes a base check for the possible amount
function RF_AB.Strategies.GuildBestDeal.createOrderEntry(price, itemConfig, totalStackSize, index, stack)
    if (price <= (itemConfig['priceGuild'] or 0)) then
        local possibleBuyStack = itemConfig['toBuyGuild']
        -- adjust amount if there is a possivle best deal
        if (itemConfig['toBuyGuild'] == nil) then
            RF_AB.Utils.error('BestDeal: can not get "toBuyGuild" for ' .. itemConfig.name)
            
            return nil
        end

        if totalStackSize < itemConfig['toBuyGuild'] * 10 then
            possibleBuyStack = itemConfig['toBuyGuild'] * 10
            --RF_AB.Utils.hint('BestDeal: possible best deal: adjusting amount from ' .. itemConfig['toBuyGuild'] .. ' to ' ..
            --        possibleBuyStack .. ' for ' .. itemConfig.itemLink)
        end
        if (totalStackSize < possibleBuyStack) then
            local amountWantedOrig = (itemConfig['toBuyGuild'] - totalStackSize)
            if (amountWantedOrig < 0) then amountWantedOrig = 0 end
            local amountWanted = (possibleBuyStack - totalStackSize)
            if (amountWanted < 0) then amountWanted = 0 end

            --local amountString = amountWantedOrig
            --if (amountWanted ~= amountWantedOrig) then amountString = amountString .. 'x / ' .. amountWanted end
            --RF_AB.Utils.success('BestDeal: preordering ' .. amountString .. 'x ' .. itemConfig.itemLink)

            return {
                amount = amountWanted,
                price = price,
                itemLink = itemConfig.itemLink,
                index = index,
                stack = stack
            }
        else
            RF_AB.Utils.hint('BestDeal: Not preordering ' .. itemConfig.itemLink .. ' enough in stock.')
        end
    end
end

function RF_AB.Strategies.GuildBestDeal.getItemFields(item_name)
    return {
        RF_AB.Settings.getItemFieldByContext('Guild', item_name, 'Price'),
        RF_AB.Settings.getItemFieldByContext('Guild', item_name, 'Amount')
    }
end
