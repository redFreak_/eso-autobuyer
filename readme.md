# Disclaimer
## The Elder Scrolls®
This Add-on is not created by, affiliated with or sponsored by ZeniMax Media Inc. or its affiliates. The Elder Scrolls® and related logos
 are registered trademarks or trademarks of ZeniMax Media Inc. in the United States and/or other countries. All rights reserved. Diese 
## AwesomeGuildStore
This Addon is neither by the authors of AwsomeGuildStore nor is it supported by them.

# known bugs
- the seems to be some issues with the settings. they are to big. 

# ToDo
- Add support for MM/TTC/ATT in description
    - TamrielTradeCentrePrice:GetPriceInfo(itemLink)
    - MasterMerchant:itemStats(itemLink, clickable)
    - ArkadiusTradeToolsSales:GetItemSalesInformation(itemLink, fromTimeStamp, allQualities)
- let the addon remember what is bought since refresh
- register dialog on tooltip and chat
- better logging by debug lvls
- todo: more categories for "other" - see alos wiki.esoui.com
- todo: buying strategies for guild

# ChangeLog
## 0.3.X - BETA-Versions
### 0.3.1
- feat: select a strategy by item for guild buys
## 0.2.X - BETA-Versions
### 0.2.19
- feat: supporting also Misc-Type
### 0.2.18
- FIX: brought to current API Version
### 0.2.17
- FIX: the fields are displayed to high for store item settings
- FIX: Enable/Disable in dialog
### 0.2.16:
- right click menu on itemLink
### 0.2.15:
- buying of guild stuff
### 0.2.14:
- categorize items in settings
### 0.2.13:
- settings devided by context
### 0.2.12:
- sorting of table
